package gui;

import ro.tuc.tp.logica.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.*;

public class GUI {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                GUI window = new GUI();
                window.initialize();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }

    public void initialize() {
        JFrame frame = new JFrame();
        frame.setTitle("Calculator polinoame");
        frame.setBounds(100, 100, 400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setVisible(true);


        JTextField p1 = new JTextField();
        p1.setText("");
        p1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 20));
        p1.setBounds(50, 25, 300, 45);
        frame.getContentPane().add(p1);
        p1.setColumns(10);

        JTextField p2 = new JTextField();
        p2.setText("");
        p2.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 20));
        p2.setBounds(50, 100, 300, 45);
        frame.getContentPane().add(p2);
        p2.setColumns(10);

        JLabel labelP1 = new JLabel();
        labelP1.setText("P1: ");
        labelP1.setBounds(15, 35, 25, 25);
        frame.getContentPane().add(labelP1);


        JLabel labelP2 = new JLabel();
        labelP2.setText("P2: ");
        labelP2.setBounds(15, 110, 25, 25);
        frame.getContentPane().add(labelP2);


        JButton add = new JButton();
        add.setText("+");
        add.setBounds(77, 180, 50, 25);
        add.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
        add.setHorizontalAlignment(JButton.CENTER);
        add.setVerticalAlignment(JButton.CENTER);
        frame.add(add);

        JButton sub = new JButton();
        sub.setText("-");
        sub.setBounds(142, 180, 50, 25);
        sub.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
        sub.setHorizontalAlignment(JButton.CENTER);
        sub.setVerticalAlignment(JButton.CENTER);
        frame.add(sub);

        JButton mul = new JButton();
        mul.setText("*");
        mul.setBounds(208, 180, 50, 25);
        mul.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
        mul.setHorizontalAlignment(JButton.CENTER);
        mul.setVerticalAlignment(JButton.CENTER);
        frame.add(mul);

        JButton div = new JButton();
        div.setText("/");
        div.setBounds(272, 180, 50, 25);
        div.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
        div.setHorizontalAlignment(JButton.CENTER);
        div.setVerticalAlignment(JButton.CENTER);
        //div.setEnabled(false);
        frame.add(div);


        JTextField result = new JTextField();
        result.setEditable(false);
        result.setBounds(50, 280, 300, 45);
        result.setHorizontalAlignment(JTextField.CENTER);
        frame.getContentPane().add(result);
        result.setColumns(10);

        JButton der = new JButton();
        der.setText("Derivare P1");
        der.setBounds(75, 225, 100, 25);
        der.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        der.setHorizontalAlignment(JButton.CENTER);
        der.setVerticalAlignment(JButton.CENTER);
        frame.getContentPane().add(der);

        JButton integ = new JButton();
        integ.setText("Integrare P1");
        integ.setBounds(225, 225, 100, 25);
        integ.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        integ.setHorizontalAlignment(JButton.CENTER);
        integ.setVerticalAlignment(JButton.CENTER);
        frame.getContentPane().add(integ);

        ArrayList<JButton> btn = new ArrayList<JButton>();
        btn.add(add);
        btn.add(sub);
        btn.add(div);
        btn.add(mul);
        btn.add(der);
        btn.add(integ);

        for (JButton aux : btn) {
            AtomicInteger ok = new AtomicInteger(1);
            aux.addActionListener(e -> {
                Polinom pol1 = new Polinom(p1.getText());
                Polinom pol2 = new Polinom(p2.getText());
                Polinom op = IOperation.getInstance(aux.getText()).compute(pol1, pol2);
                ResultType divop = new ResultType();
                if(aux.equals(div)) {
                    if(p2.getText().equals("") || p2.getText().equals("0") || p2.getText().equals("0x^0") || p2.getText().equals("0x^")) {
                        JOptionPane.showMessageDialog(frame, "Polinom nul", "Eroare", JOptionPane.ERROR_MESSAGE);
                        ok.set(0);
                    }
                    else
                        divop = new DivOp().computeR(pol1, pol2);
                }


                Collections.sort(op.getElem(), new Comparator<Monom>() {
                    @Override
                    public int compare(Monom a, Monom b) {
                        return b.getGrad() - a.getGrad();
                    }
                });

                if(aux.equals(div) && ok.get() == 1)
                    result.setText(divop.toString());
                else
                    result.setText(op.toString());

            });
        }
    }
}
