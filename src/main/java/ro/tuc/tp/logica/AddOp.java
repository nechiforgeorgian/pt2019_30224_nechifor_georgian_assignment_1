package ro.tuc.tp.logica;

import java.util.*;

public class AddOp implements IOperation {

    public Polinom compute(Polinom p1, Polinom p2) {
        Polinom rez = new Polinom("");
        rez.getElem().addAll(p1.getElem());
        for (Monom j : p2.getElem()) {
            rez.addMonom(new Monom(j.getCoef(), j.getGrad()));
        }
        return rez;
    }
}
