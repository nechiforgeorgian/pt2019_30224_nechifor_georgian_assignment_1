package ro.tuc.tp.logica;

public class DerOp implements IOperation {
    public Polinom compute(Polinom p1, Polinom p2) {
        Polinom rez = new Polinom("");
        for(Monom i: p1.getElem()) {
            rez.addMonom(new Monom(i.getCoef() * i.getGrad(), i.getGrad() - 1));
        }
        return rez;
    }
}
