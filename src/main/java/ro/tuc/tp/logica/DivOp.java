package ro.tuc.tp.logica;

public class DivOp implements IOperation  {

    public ResultType computeR(Polinom p1, Polinom p2) {
        int grad1 = p1.getGrad();
        int grad2 = p2.getGrad();
        Polinom copy = p1;

        if(grad1 < grad2)
            return null;

        Polinom rezultat = new Polinom("");
        while(grad1 >= grad2) {
            Monom l1 = copy.getLead();
            Monom l2 = p2.getLead();

            Monom t = new Monom(l1.getCoef() / l2.getCoef(), l1.getGrad() - l2.getGrad());
            Polinom i = new Polinom("");
            i.addMonom(new Monom(-t.getCoef(), t.getGrad()));
            rezultat.addMonom(t);
            Polinom aux = new MulOp().compute(p2, i);
            copy = new AddOp().compute(copy, aux);

            grad1 = copy.getGrad();
        }

        return new ResultType(rezultat, copy);
    }
    @Override
    public Polinom compute(Polinom p1, Polinom p2) {
        return new Polinom("");
    }
}


//6x^3+2x^2+8x^1+16x^0
//2x^2-3x^1+4x^0

//4x^2-4x^0
//2x^1-2x^0