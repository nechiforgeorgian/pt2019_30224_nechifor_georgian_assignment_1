package ro.tuc.tp.logica;

public interface IOperation {
    Polinom compute(Polinom p1, Polinom p2);

    static IOperation getInstance(String s) {
        switch (s) {
            case "+":
                return new AddOp();
            case "-":
                return new SubOp();
            case "*":
                return new MulOp();
            case "/":
                return new DivOp();
            case "Derivare P1":
                return new DerOp();
            case "Integrare P1":
                return new IntegOp();
            default:
                return null;
        }
    }
}
