package ro.tuc.tp.logica;

public class IntegOp implements IOperation {
    @Override
    public Polinom compute(Polinom p1, Polinom p2) {
        Polinom rez = new Polinom("");
        for(Monom i: p1.getElem()) {
            rez.addMonom(new Monom((float)i.getCoef() / (i.getGrad()+1), i.getGrad()+1));
        }

        return rez;
    }
}
