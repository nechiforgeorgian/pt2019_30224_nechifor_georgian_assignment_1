package ro.tuc.tp.logica;

public class Monom {
    private int grad;
    private float coef;

    public Monom() {
        grad = 0;
        coef = 0;
    }

    public Monom(float coef, int grad) {
        this.coef = coef;
        this.grad = grad;
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public float getCoef() {
        return coef;
    }

    public void setCoef(float coef) {
        this.coef = coef;
    }

    public String toString() {
        if (coef == 0.0)
            return "";
        else if (grad == 0)
            return coef + "";
        else
            return coef + "x^" + grad;
    }
}
