package ro.tuc.tp.logica;

public class MulOp implements IOperation {

    public Polinom compute(Polinom p1, Polinom p2) {
        Polinom result = new Polinom("");
        for(Monom i: p1.getElem()) {
            for(Monom j: p2.getElem()) {
                result.addMonom(new Monom(j.getCoef() * i.getCoef(), j.getGrad() + i.getGrad()));
            }
        }
        return result;
    }
}
