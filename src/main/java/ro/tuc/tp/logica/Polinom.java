package ro.tuc.tp.logica;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
    private List<Monom> elem = new ArrayList<Monom>();

    public List<Monom> getElem() {
        return elem;
    }

    public Polinom(String s) {
        Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
        Matcher m = p.matcher(s);
        while (m.find()) {
            this.addMonom(new Monom(Float.parseFloat(m.group(1)), Integer.parseInt(m.group(2))));
        }
    }

    public void addMonom(Monom m) {
        boolean found = false;
        for (Monom i : elem) {
            if (i.getGrad() == m.getGrad()) {
                i.setCoef(i.getCoef() + m.getCoef());
                found = true;
                break;
            }
        }
        if (!found)
            elem.add(m);
    }

    public int getGrad() {
        int max = 0;
        for(Monom m: elem) {
            if(!m.toString().equals("") && m.getGrad() > max)
                max = m.getGrad();
        }
        return max;
    }

    public Monom getLead() {
        Monom m = new Monom(0, 0);
        for(Monom i : elem) {
            if(!i.toString().equals("") && i.getGrad() >= m.getGrad()) {
                m.setGrad(i.getGrad());
                m.setCoef(i.getCoef());
            }
        }

        return m;
    }

    public boolean emptyList() {
        for(Monom m: elem) {
            if(m.getCoef() != 0)
                return false;
        }
        return true;
    }

    public String toString() {
        String rez = "";
        for (Monom m : elem) {
            rez += m.toString();
            if (!m.toString().equals(""))
                rez += " + ";
        }
        char c = ' ';
        StringBuilder s = new StringBuilder(rez);
        s.deleteCharAt(s.length() - 3);
        s.deleteCharAt(s.length() - 2);
        s.deleteCharAt(s.length() - 1);
        rez = s.toString();
        return rez;
    }
}
