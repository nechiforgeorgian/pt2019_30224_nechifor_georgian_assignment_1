package ro.tuc.tp.logica;

import javax.xml.transform.Result;

public class ResultType {
    Polinom p = new Polinom("");
    Polinom r = new Polinom("");

    public ResultType() {}

    public ResultType(Polinom p, Polinom r) {
        this.p = p;
        this.r = r;
    }

    public Polinom getP() {
        return p;
    }

    public void setP(Polinom p) {
        this.p = p;
    }

    public Polinom getR() {
        return r;
    }

    public void setR(Polinom r) {
        this.r = r;
    }

    public String toString() {
        if(!r.emptyList())
            return "C: " + p + "; R: " + r;
        return "C: " + p;
    }
}