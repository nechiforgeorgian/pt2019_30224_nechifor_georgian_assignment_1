package ro.tuc.tp.logica;

import java.util.Collections;
import java.util.Comparator;

public class SubOp implements IOperation {

    public Polinom compute(Polinom p1, Polinom p2) {
        Polinom result = new Polinom("");
        result.getElem().addAll(p1.getElem());
        for (Monom j : p2.getElem()) {
           result.addMonom(new Monom( -j.getCoef(), j.getGrad()));
        }

        return result;
    }
}
