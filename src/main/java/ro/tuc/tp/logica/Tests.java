package ro.tuc.tp.logica;

import junit.framework.*;

import java.util.Collections;
import java.util.Comparator;

import org.junit.Test;

class sorting {
	
	public void sortPol(Polinom op) {
	 Collections.sort(op.getElem(), new Comparator<Monom>() {
         @Override
         public int compare(Monom a, Monom b) {
             return b.getGrad() - a.getGrad();
         }
     });
	}
}

public class Tests extends TestCase {
    public Polinom p1, p2;

    public void setUp() {
        p1 = new Polinom("2x^2+3x^1+5x^0");
        p2 = new Polinom("3x^3+4x^2+1x^1-3x^0");
    }

    public void testAdd() {
        Polinom res = new AddOp().compute(p1, p2);
        Polinom p3 = new Polinom("3x^3+6x^2+4x^1+2x^0");
        new sorting().sortPol(res);
        int i = 0;
     
        int ok = 1;
        for(Monom m: res.getElem()) {
        	Monom p = p3.getElem().get(i);
        	if(m.getCoef() == p.getCoef() && m.getGrad() == p.getGrad())
        		ok = 1;
        	else ok = 0;
        	i++;
        }
        assertTrue(ok == 1);
    }
    
    public void testSub() {
        Polinom res = new SubOp().compute(p1, p2);
        Polinom p3 = new Polinom("-3x^3-2x^2+2x^1+8x^0");
        new sorting().sortPol(res);
        int i = 0;
        int ok = 1;
        for(Monom m: res.getElem()) {
        	Monom p = p3.getElem().get(i);
        	if(m.getCoef() == p.getCoef() && m.getGrad() == p.getGrad())
        		ok = 1;
        	else ok = 0;
        	i++;
        }

        assertTrue(ok == 1);
    }

    public void testMul() {
        Polinom res = new MulOp().compute(p1, p2);
        Polinom p3 = new Polinom("6x^5+17x^4+29x^3+17x^2-4x^1-15x^0");
        new sorting().sortPol(res);
        int i = 0;
        int ok = 1;
        int finalstate = 1;
        for(Monom m: res.getElem()) {
        	Monom p = p3.getElem().get(i);
        	if(m.getCoef() == p.getCoef() && m.getGrad() == p.getGrad())
        		ok = 1;
        	else finalstate = 0;
        	i++;
        }

        assertTrue(finalstate == 1);
    }

    public void testDiv() {

    	ResultType res = new DivOp().computeR(p2, p1);
        Polinom cat = new Polinom("");
        cat.addMonom(new Monom((float)1.5, 1));
        cat.addMonom(new Monom((float)-0.25, 0));
        Polinom rest = new Polinom("");
        rest.addMonom(new Monom((float) -5.75, 1));
        rest.addMonom(new Monom((float) -1.75, 0));


        int i = 0;
        int ok = 1;
        int finalstate = 1;
        for(Monom m: res.getP().getElem()) {
        	Monom p = cat.getElem().get(i);
        	if(!m.equals("") && m.getCoef() == p.getCoef() && m.getGrad() == p.getGrad()) {
        		ok = 1;
        	}
        	else
        		finalstate = 0;
        	i++;
        }

        assertTrue(finalstate == 1);
    }
}
